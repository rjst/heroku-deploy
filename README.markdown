Heroku Deploy from Bitbucket Pipelines
======================================

Simple bash script to deploy to Heroku from Bitbucket Pipelines.

Usage:
------
1. Copy heroku-deploy.sh to the root of your repository
2. Configure the following environment variables in Bitbucket Pipelines
    - HEROKU_API_KEY - https://devcenter.heroku.com/articles/platform-api-quickstart
    - HEROKU_APP_NAME - Your app name in Heroku, e.g. 'app-name-1234'
3. Configure this script to execute in your build step.  See the example [bitbucket-pipelines.yml](bitbucket-pipelines.yml)

Troubleshooting
---------------
* Double-check that the script is committed to your repository with execute permissions

License: MIT

